import Vue from 'vue'
import App from './App.vue'

Vue.directive('myon', {
    bind(el, binding, vnode) {
        console.log('Event: ', binding.arg);
        console.log('Function to call: ', binding.value);
        el.addEventListener(binding.arg, binding.value);
    }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
