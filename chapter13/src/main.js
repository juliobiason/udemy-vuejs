import Vue from 'vue'
import App from './App.vue'

// NOTE: Registering global functions like this doesn't work.
function strReverse(str) {
    return str.split('').reverse().join('');
    // NOTE: got from here: http://stackoverflow.com/questions/958908/how-do-you-reverse-a-string-in-place-in-javascript#959004
}

Vue.filter('char-count', function(value) {
    return value + ' (' + value.length + ')';
});

// NOTE: Could've used mixin = { ... } instead of using Vue.mixing(), but since
//       it was already here, I thought I would be better to use it instead.
Vue.mixin({
    data() {
        return {
            text: 'Text from a mixing'
        }
    },
    created() {
        console.log('Global Mixin - Created Hook');
    },
    computed: {
        reversedText() {
            return this.text.split('').reverse().join('') +
                ' (' + this.text.length + ')';
        }
    }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
